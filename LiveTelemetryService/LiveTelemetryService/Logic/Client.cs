﻿using KafkaConsumer.Core.Models;
using KafkaConsumer.Core.Consumer;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading;
using System.Threading.Channels;

namespace LiveTelemetryService.Logic
{
    public class Client : Hub
    {
        private ChannelWriter<FrameData> _writer;
        private CancellationTokenSource _cancellationToken;
        private ConsumerRunner<FrameData> _kafkaConsumer;

        public ChannelReader<FrameData> Counter(string topic)
        {
            var channel = Channel.CreateUnbounded<FrameData>();
            _writer = channel.Writer;
            _cancellationToken = new CancellationTokenSource();

            _kafkaConsumer = new ConsumerRunner<FrameData>(WriteDataAsync, _cancellationToken);
            _kafkaConsumer.Subscribe(topic);

            return channel.Reader;
        }

        private async void WriteDataAsync(FrameData frame)
        {
            try
            {
                Console.WriteLine($"Frame consumed at {frame.Header.Timestamp}, {frame.Counter}");
                await _writer.WriteAsync(frame, _cancellationToken.Token);
            }
            catch (Exception)
            {
            }
        }

        protected override void Dispose(bool disposing)
        {
            _cancellationToken?.Cancel();
            _cancellationToken?.Dispose();
            _kafkaConsumer?.Dispose();
        }
    }
}
