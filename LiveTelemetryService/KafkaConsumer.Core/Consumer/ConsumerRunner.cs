﻿using Confluent.Kafka;
using KafkaConsumer.Core.Models;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace KafkaConsumer.Core.Consumer
{
    public class ConsumerRunner<T> : IDisposable
    {
        private const string USERNAME = "rwdadoqj";
        private const int SIMULATION_INTERVAL = 70;

        private readonly ConsumerConfig _config;
        private readonly IConsumer<Ignore, string> _consumer;

        private readonly Action<T> _onConsumed;
        private readonly CancellationTokenSource _cancellationToken;


        public ConsumerRunner(Action<T> onDelivery, CancellationTokenSource cancellationTokenSource)
        {
            _config = new ConsumerConfig
            {
                BootstrapServers = "dory.srvs.cloudkafka.com:9094",
                SaslUsername = $"{USERNAME}",
                SaslPassword = "i1A6Yx8wCvXx0npAflL3AK_mQQoe0sfY",
                SaslMechanism = SaslMechanism.ScramSha512,
                SecurityProtocol = SecurityProtocol.SaslSsl,
                Acks = Acks.All,
                AutoOffsetReset = AutoOffsetReset.Latest,
                GroupId = GenerateGroupId()
            };

            _onConsumed = onDelivery;
            _cancellationToken = cancellationTokenSource;
            _consumer = new ConsumerBuilder<Ignore, string>(_config).Build();
        }

        private string GenerateGroupId()
        {
            string randomString = Guid.NewGuid().ToString("N").Substring(0, 9); // Generate a random string using Guid and take the first 8 characters
            return $"{USERNAME}-{randomString}";
        }

        public void Subscribe(string topic)
        {
            _consumer.Subscribe($"{USERNAME}-{topic}");
            StartConsume();
        }

        private void StartConsume()
        {
            Task.Run(() =>
            {
                while (!_cancellationToken.IsCancellationRequested)
                {
                    var consumeResult = _consumer.Consume(_cancellationToken.Token);
                    T frame = JsonSerializer.Deserialize<T>(consumeResult.Message.Value);
                    _onConsumed?.Invoke(frame);
                    Thread.Sleep(SIMULATION_INTERVAL);
                }
            });
        }

        public void Dispose()
        {
            _consumer?.Unsubscribe();
            _consumer?.Close();
            _consumer?.Dispose();
        }
    }
}
