﻿using System;

namespace KafkaConsumer.Core.Models
{
    public class Headers
    {
        public DateTime Timestamp { get; set; }
        public string TailNumber { get; set; }

        public Headers()
        {

        }

        public Headers(string tailNumber = "000")
        {
            Timestamp = DateTime.Now;
            TailNumber = tailNumber;
        }
    }
}
