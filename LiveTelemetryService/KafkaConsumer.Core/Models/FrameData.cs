﻿using System.Collections.Generic;

namespace KafkaConsumer.Core.Models
{
    public class FrameData
    {
        public int Counter { get; set; }
        public List<MessageElement> Parameters { get; set; }
        public Headers Header { get; set; }

        public FrameData()
        {
            Counter = 0;
            Parameters = new List<MessageElement>();
            Header = new Headers();
        }
    }
}
