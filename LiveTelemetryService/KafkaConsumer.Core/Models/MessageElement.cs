﻿namespace KafkaConsumer.Core.Models
{
    public class MessageElement
    {
        public string ParameterName { get; set; }
        public int Value { get; set; }

        public MessageElement()
        {

        }
    }
}
